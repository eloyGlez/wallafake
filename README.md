# WallaFake

Proyecto de Angular basado en un catálogo de productos consumidos desde una API(custom), segmentado en distintas páginas que nos permitan una navegación e interacción con dicha API y con los productos de la misma.

El diseño base, será el siguiente:
    https://www.figma.com/file/6ZheZRH5UiHMC6tw9F4kQl/Shopéame

## Instalación / Arranque Proyecto
    npm i
    npm build
    npm run dev

## Cambios en el fichero de config
    Added to webpack.config:
    favicon: "./src/favicon.ico",


## Estructura del proyecto
El proyecto se segmentará en tres páginas con sus componentes correspondientes.

#HOME
#PRODUCTS
#MANAGEMENT

Todas las páginas tienen en común dos componentes:

-HEADER: Contendrá el logo de la aplicación y los enlaces del menú principal. Recuerda que debe indicar en qué página te   encuentras en cada momento.

-FOOTER: En principio solo contendrá unos enlaces a redes sociales. Para el aviso de Copyright te aconsejamos que no pongas el año en el template "a fuego", para que cuando llegue 2021 cambie automáticamente.
_____________________________________________________________________

Las páginas en cuestión tendrán la siguiente estructura:

-HOME: Constará de un componente en el que mostraremos una pequeña introducción/descripción de la aplicación, junto con el logo.


-PRODUCTS: Debe mostrar la lista de productos disponibles. Tendrá un pequeño buscador que filtre los productos por su nombre.
El listado de productos se obtendrá desde la siguiente API mediante un GET:
    https://my-json-server.typicode.com/franlindebl/shopeame-api-v2/products
Además podremos modificar la vista de los productos ya sea en cuadrícula o lista.
Estos productos se basarán en un componente plantilla que nos permita pintar tantos productos como sea posible.


-MANAGEMENT: Página de gestión de productos, en la cual el usuario podrá añadir productos nuevos al catálogo y editar los ya existentes. Todo ello mediante un formulario con sus validaciones y mostrando una preview en uno de los laterales del producto que se está editando o añadiendo.
_____________________________________________________________________

### Requisitos generales

En nuestra aplicación deberemos tener los siguientes requisitos técnicos:

1. Tipado: hacer uso de intefaces para tipar los elementos de nuestra aplicación. Está prohibido el uso de tipo object o any.
2. Distribución de la aplicación en módulos. La aplicación deberá contar con al menos dos módulos, tal y como os hemos indicado con anterioridad uno será el shared.module, lo correcto es que uses además de este, un módulo por página también.
3. Formularios reactivos: Deberás hacer uso de los formularios reactivos y sus validaciones con mensajes de error
4. Router: Deberás hacer uso del router y aplicar carga dinámica de módulos a través del lazy loading.
5. Uso de un pipes, por ejemplo para el componente de filtrado o para mostrar los nombres
6. Testing: Integrar test unitarios en al menos un componente, aunque es deseable que testemos todos.